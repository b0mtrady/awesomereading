## Important Papers In The History of ML

(1943) A Logical calculus of the ideas immanent in nervous activity - Mculloh & Pitts

(1950) [Computing Machinery and Intelligence](https://www.csee.umbc.edu/courses/471/papers/turing.pdf) - Alan Turing - "Can Machines Think Paper - The Imitation Game"

(1950) The Human Use Of Human Beings - Norbert Wiener

## Repos

[Spacy](https://github.com/explosion/spaCy) - NLP library in Python and Cython

[StyleGan](https://github.com/NVlabs/stylegan) from Nvidia - SotA in 2019 - TensorFlow implementation 

[CTRL - A Conditional Transformer Language Model for Controllable Generation](https://github.com/salesforce/ctrl) - Now in hugginface/transformers

[Pysyft](https://github.com/OpenMined/PySyft) -  "A library for encrypted, privacy preserving machine learning. PySyft is a Python library for secure, private Deep Learning. PySyft decouples private data from model training, using Federated Learning, Differential Privacy, and Multi-Party Computation (MPC) within PyTorch." 

[TensorFlow Federated](https://github.com/tensorflow/federated)
an open-source framework for machine learning and other computations on decentralized data. TFF has been developed to facilitate open research and experimentation with Federated Learning (FL), an approach to machine learning where a shared global model is trained across many participating clients that keep their training data locally. 

[Cleverhans](https://github.com/tensorflow/cleverhans) - a Python library to benchmark machine learning systems' vulnerability to adversarial examples.
## Training Material (In Best Learning Order)

[Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/index.html) - Michael Nielsen

[The Mechanics of Machine Learning](https://mlbook.explained.ai/)

[Deep Learning](https://www.deeplearningbook.org/) - Goodfellow, Bengio, Courville

[Reinforcement Learning: An Introduction](http://incompleteideas.net/book/the-book.html) - Sutton and Barto's great book

[Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html) - Abelson & Sussman

[Natural Language Processing with Python](https://www.nltk.org/book/)

[The Elements of Statistical Learning](https://web.stanford.edu/~hastie/ElemStatLearn//printings/ESLII_print10.pdf)

[Spinning Up In Deep Reinforcement Learning - OpenAi](https://spinningup.openai.com/en/latest/index.html)

[Advanced Bash Scripting](http://www.tldp.org/LDP/abs/html/abs-guide.html) - Book on how to use the shell

### Videos 

[Derivatives Introduction](https://youtu.be/QqF3i1pnyzU) - Nancy Pi video

[Partial Derivatives](https://youtu.be/rnoToCoEK48) - Krista King Video 

[The Chain Rule Explain](https://youtu.be/H-ybCx8gt-8) - Great Nancy Pi video 

[More Chain Rule](https://youtu.be/U_qp0isxQYU) - Nancy Pi

[Introduction To Log](https://youtu.be/ntBWrcbAhaY) - Eddie WooVideo

[What Are Embeddings?](https://developers.google.com/machine-learning/crash-course/embeddings/video-lecture) - Google Embeddings Tutorial

## Posts 

(2015) [The Unreasonable Effectiveness of Recurrent Neural Networks](http://karpathy.github.io/2015/05/21/rnn-effectiveness/) - Karpathy's famous blog article

(2018) [Setting The Learning Rate Of Your Neural Network](https://www.jeremyjordan.me/nn-learning-rate/) - Describes Leslie Smith's Cyclical Learning Rate Technique

(2019) [OpenAi Full Release of GPT-2 Model](https://openai.com/blog/gpt-2-1-5b-release/)

(2019) [Machine Learning With Differential Privacy in TensorFlow](http://www.cleverhans.io/privacy/2019/03/26/machine-learning-with-differential-privacy-in-tensorflow.html) - CleverHans post on differential privacy in ML

## FastAi

(2019) [Understanding Callbacks in Fastai](https://pouannes.github.io/blog/callbacks-fastai/) - Explains callbacks in different parts of the training loop

## Tools

[Python 3.6+](https://www.python.org/) -Open Source OSI [License](https://docs.python.org/3/license.html)
[FastAi](https://github.com/fastai/fastai) - Open Source Apache2.0 [License](https://github.com/fastai/fastai/blob/master/LICENSE)

[Pytorch](https://pytorch.org/) Open Source BSD [License](https://github.com/pytorch/pytorch/blob/master/LICENSE)

[TensorFlow](https://github.com/tensorflow/tensorflow) - Open Source Apache 2.0 [License](https://github.com/tensorflow/tensorflow/blob/master/LICENSE)

[Numpy](https://numpy.org/) - Open Source BSD [License](https://numpy.org/license.html)

# Machine Learning Architecture 
Controlling The Data Pipeline

Compliance - example (GDPR)
Privacy 
### CI/CD

#### Version Control
 - For History - too see what worked and didnt
 - Onboarding - To help new team members get up to speed 
 - Debugging - To figure out what went wrong
 - For Improved Models - this is essential for taking the model in different directions; brances and master
 - For Traceability (Liability) - to determine why model made specific decisions which is important for many reason not least of which are legal considerations. 

#### Continuous Improvement

CI ML requires an iterative plan for maintaining production models. A model that isn't monitored and managed for performance degrades into irrelavance over time. 

Causes of Model Degradation
 - Security Vulnerabilities  - (Adversarial Attacks - example CleverHans)
 - Concept Drift (aka Model Drift) 

##### Steps to Monitor and Maintain Model Performance
 - Regular schedule of model review and model retrain
 - Performance Traces (metrics comparisons)
 - Weighting data input by recency or age. Higher weight to recent data. 
##### Concept Drift
The statistical properties of the target variable, which the model is trying to predict, change over time in unforeseen ways. This causes problems because the predictions become less accurate as time passes.

[Continuous Integration since 2006](https://www.jamesshore.com/Blog/Continuous-Integration-on-a-Dollar-a-Day.html) - Ci/CD on a dollar a day

#### Articles 

[Microservices Increases Operational Complexity: Does The Organization Have The Culture To Handle It?](https://dwmkerr.com/the-death-of-microservice-madness-in-2018/) 
 - Many organisations still run with separated development and operations teams - and a organisation that does is much more likely to struggle with adoption of microservices.
 - Yes, with effective automation, monitoring, orchestration and so on, this is all possible. But the challenge is rarely the technology - the challenge is finding people who can use it effectively.
 - This is where things can get extremely complex. If your boundaries are actually not well defined, then what happens is that even though theoretically services can be deployed in isolation, you find that due to the inter-dependencies between services, you have to deploy sets of services as a group. 


## The Future

### MLIR
[Lattner Presentation on MLIR in 2019](https://storage.googleapis.com/pub-tools-public-publication-data/pdf/1c082b766d8e14b54e36e37c9fc3ebbe8b4a72dd.pdf)
